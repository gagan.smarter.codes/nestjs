# Installation:-
Open a new terminal and run:-
```
npm i -g @nestjs/cli
```

# Crate a new Project
Let's create a new app `demo` by running the following command at the directory where you want to have your project.
```
nest new demo
```

# Project Structure
A new project inside `demo` directory is already created. Move into `src` directory and you will see these files:-
```txt
src
|-app.controller.ts
|-app.controller.spec.ts
|-app.service.ts
|-app.module.ts
|-main.ts
```
Let's go through these files one by one.

## app.controller.ts
This file contains controller with single route and has following:-
```typescript
import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
}
```
Here, we used `controller` and defining a `GET` method called `getHello` using `@Get` decorator which is calling `AppService` from `app.service.ts`.

## app.controller.spec.ts
This file contains test cases for the controller. Let's explore it:-
```typescript
import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [AppService],
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(appController.getHello()).toBe('Hello World!');
    });
  });
});

```
Here, `beforeEach` method is used to initialize some methods or modules before each test case. 
Each test module is described by `describe` method and each test case is declared by `it`. A single test module can contain multiple test cases. A test module can be visualized as a single functionality we want to test while each test cases inside it will be the scenarios we want to test for this functionality.

## app.service.ts
This file contains funtions which are called by `app,controller.ts`. Ideally, this file should contain the logic/functionality of the module. Let's examine the it's content:-
```typescript
import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }
}

```

Here, we declared a method component class which has a function called `getHello`. Note that this is the same function which we are using in controller.

## app.module.ts
This file act as the entry-point for `app` module. It contains following code:-
```typescript
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

```
Here we created a module class `AppModule` using `@Module` decorator from nest-js. The decorator provide several arguments like `controllers` where we can declare our controllers, `providers` which has `service` component. Similarly, we can also specify `imports`.

## main.ts

It is the file which is responsible for initializinga and starting the server.
```typescript
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  await app.listen(3000);
}
bootstrap();
```
As you can guess, we are calling `bootstrap` function which creates a server instance using `NestFactory` and we pass our `AppModule` inside it. After creating it, we begins to listen on `3000` port on our local machine.

# Running our Project
Open a terminal and run the following command,
```
npm run start
```
If you wish to listen for file changes(which you usually want during development), use the following command:-
```
npm run start:dev
```
Open your browser and move to [3000](http://localhost:3000) port.
![Our Application is running](/images/Tutorials/NestJs/Demo.jpg)

# Controllers
As we discussed earlier, controllers are used for routing, and returning appropiate responses. 
## Create a New Controller
Let's say we want to create a new controller called `message` which is supposed to handle all the requests with `/messages` url.
There are two ways to create a new controller:-
1. Create the controller manually.
2. Use following command to create a controller:-
	```txt
	nest g controller message
	```
We will use the second method even though the choice does not affect the rest of the steps.
Two new files in `src` should be visible.
```txt
src
|-message
    |-message.controller.ts
    |-message.controller.spec.ts
```
## Create a new Route
Open `message.controller.ts`. It will look like this:-
```typescript
import { Controller } from '@nestjs/common';

@Controller('message')
export class MessageController {}
```
Here `message` argument to `Controller` means this controller will handle all the routes with `/messages`. Let's create a method to handle `GET` method at `/messages`. It can be done in this manner:-
```typescript
import { Controller, Get } from '@nestjs/common';

@Controller('message')
export class MessageController {
    @Get()
    listAll(): string {
        return 'This page will list messages';
    }
}
```
If we visit `/message` in our browser, we will see this message.
![/messages url](/images/Tutorials/NestJs/messages.jpg)

But we have not included our controller anywhere, how is it working? Let's open `app.module.ts`:-
```typescript
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MessageController } from './message/message.controller';

@Module({
  imports: [],
  controllers: [AppController, MessageController],
  providers: [AppService],
})
export class AppModule {}

```
By default, when we are creating our controller using command, NestJs include the controller in `App` module.

## How NestJs Handles the response
There are two options for handlind responses:-
1. **Standard(recommended):-** By default, NestJs serializes object or array to JSON. However, when we send primitive types like string, number or boolean, it will send the response as it is.
2. **Library-specific:-** We can use library-specific methods by injecting them using `@Res` decorator.

> **Warning**
>
> Nest detects when the handler is using either `@Res()` or `@Next()`, indicating you have chosen the library-specific option. If both approaches are used at the same time, the Standard approach is automatically disabled for this single route and will no longer work as expected. To use both approaches at the same time (for example, by injecting the response object to only set cookies/headers but still leave the rest to the framework), you must set the passthrough option to true in the `@Res({ passthrough: true })` decorator.

## Request Object

Sometimes, we need to access client's request object, it can be done easily using `@Req`.
```typescript
import { Controller, Get, Req } from '@nestjs/common';
import { Request } from 'express';

@Controller('message')
export class MessageController {
    @Get()
    listAll(@Req() request: Request): string {
        return 'This page will list messages';
    }
}

```

Usually, we also want to provide `POST` methods to mainpulate the data.
```typescript
import { Controller, Get, Post, Req } from '@nestjs/common';
import { Request } from 'express';

@Controller('message')
export class MessageController {
    @Post()
    createMessage(): string {
        return 'adding a new message';
    }
    @Get()
    listAll(@Req() request: Request): string {
        return 'This page will list messages';
    }
}

```

## Status Code
By default, we get status `200` for `GET` request, but we can change it using `@HttpCode` like below:-
```typescript
import { Controller, Get, HttpCode, Post, Req } from '@nestjs/common';
import { Request } from 'express';

@Controller('message')
export class MessageController {
    @Post()
    @HttpCode(208)
    createMessage(): string {
        return 'adding a new message';
    }
    @Get()
    listAll(@Req() request: Request): string {
        return 'This page will list messages';
    }
}
```

## Headers
Use `@Header` to specify the headers,
```
import { Controller, Get, Header, HttpCode, Post, Req } from '@nestjs/common';
import { Request } from 'express';

@Controller('message')
export class MessageController {
    @Post()
    @Header('Cache-Control', 'none')
    @HttpCode(208)
    createMessage(): string {
        return 'adding a new message';
    }
    @Get()
    listAll(@Req() request: Request): string {
        return 'This page will list messages';
    }
}

```

## Redirect
Use `@Redirect` to specify where you want to specify,note that `url` as well as `statusCode` are optional.
```typescript
@Redirect('http://localhost:3000/profile',200);
```
Sometimes, you want to redirect the user dynamically based on some condition,you can return an object with this structure to do so,
```typescript
{
	"url":"http://localhost:3000/profile",
	"statusCode":200
}
```

## Route Params
Suppose, we want to get `id` of the message from the url, we need to use `@Param` header to do so.
```typescript
import { 
	Controller, 
	Get, 
	Header, 
	HttpCode, 
	Param, 
	Post, 
	Req } 
from '@nestjs/common';
import { Request } from 'express';

@Controller('message')
export class MessageController {
    
    @Post()
    @Header('Cache-Control', 'none')
    @HttpCode(208)
    createMessage(): string {
        return 'adding a new message';
    }
    
    @Get()
    listAll(@Req() request: Request): string {
        return 'This page will list messages';
    }
    
    @Get(':id')
    viewMessage(@Param() params): string {
        return `you are viewing ${params.id} message`;
    }
}

```
Try visiting [/message/100](http://localhost:3000/message/100), you will get this response:-
![passing params from route](/images/Tutorials/NestJs/params.jpg)

You can also access `id` directly by using `Param` like that:-
```typescript
import { Controller, Get, Header, HttpCode, Param, Post, Redirect, Req } from '@nestjs/common';
import { Request } from 'express';

@Controller('message')
export class MessageController {
    
    @Post()
    @Header('Cache-Control', 'none')
    @HttpCode(208)
    createMessage(): string {
        return 'adding a new message';
    }
    
    @Get()
    listAll(@Req() request: Request): string {
        return 'This page will list messages';
    }

    @Get(':id')
    viewMessage(@Param('id') id: string): string {
        return `you are viewing ${id} message`;
    }
}

```

## Request Payloads

Our `POST` client do not accept any client params. To do so, we need to first declare **DTO(Data Transfer Object)**. Create a file called `CreateMessageDTO`, inside `/message` folder.
```typescript
export class CreateMessageDto {
    sender: String;
    body: String;
    receiver: String;
};
```
Now, we can call `MessageDto` inside our controller,
```typescript
import {
    Body,
    Controller,
    Get,
    Header,
    HttpCode,
    Param,
    Post,
    Req
} from '@nestjs/common';
import { Request } from 'express';
import { CreateMessageDto } from './create-message-DTO';

@Controller('message')
export class MessageController {

    @Post()
    @Header('Cache-Control', 'none')
    @HttpCode(208)
    async createMessage(@Body() createMessageDto: CreateMessageDto) {
        return 'adding a new message';
    }

    @Get()
    listAll(@Req() request: Request): string {
        return 'This page will list messages';
    }

    @Get(':id')
    viewMessage(@Param() params): string {
        return `you are viewing ${params.id} message`;
    }
}
```

# Providers
Providers are the methods which does some work for us. Usually, controllers are meant for handlind HTTP Requests while complex tasks are passed to Providers.

## Service
We can create a Service by two methods:-
1. Manually
2. By CLI using following command:-
   	```txt
	nest g service message
   	```
Now our `/message` subfolder will look like:-
```txt
message
|-create-message-DTO.ts
|-message.controller.ts
|-message.controller.spec.ts
|-message.service.ts
|-message.service.spec.ts
```
Our `message.service.ts` looks like:-
```typescript
import { Injectable } from '@nestjs/common';

@Injectable()
export class MessageService {}
```
We need to add some messages here, but before that we need to create a interface for `Message`. Create a file called `message.interface.ts`.
```typescript
export interface Message {
    sender: String;
    body: String;
    receiver: String;
}
```
Now using this interface, let us add some methods in out service,
```typescript
import { Injectable } from '@nestjs/common';
import { Message } from './message.interface';
@Injectable()
export class MessageService {
    private readonly messages: Message[] = [];

    create(message: Message) {
        this.messages.push(message);
    }

    listAll(): Message[] {
        return this.messages;
    }
}

```
Now, let's plug `MessageService` into `MessageController`.
```typescript
import {
    Body,
    Controller,
    Get,
    Header,
    HttpCode,
    Param,
    Post,
    Req
} from '@nestjs/common';
import { Request } from 'express';
import { CreateMessageDto } from './create-message-DTO';
import { MessageService } from './message.service';
import { Message } from './message.interface';

@Controller('message')
export class MessageController {

    //dependency injection
    constructor(private messageService: MessageService) { }

    @Post()
    @Header('Cache-Control', 'none')
    @HttpCode(208)
    async createMessage(@Body() createMessageDto: CreateMessageDto) {
        this.messageService.create(createMessageDto);
    }

    @Get()
    async listAll(@Req() request: Request): Promise<Message[]> {
        return this.messageService.listAll();
    }

    @Get(':id')
    viewMessage(@Param() params): string {
        return `you are viewing ${params.id} message`;
    }
}

```
## Optional Providers
Ocassionally, you might have some dependency which are optional depending on some condition. Then, you can use `@Optional` decorator like below:-
```typescript
import { Injectable, Optional, Inject } from '@nestjs/common';

@Injectable()
export class HttpService<T> {
  constructor(@Optional() @Inject('HTTP_OPTIONS') private httpClient: T) {}
}

```

## Property-Based Injection

If your top-level class depends on one-or-multiple providers, then, passing them all the way up by calling super is tedious. To avoid it, we can use `@Inject` decorator.
```typescript
import { Injectable, Inject } from '@nestjs/common';

@Injectable()
export class HttpService<T> {
  @Inject('HTTP_OPTIONS')
  private readonly httpClient: T;
}

```

## Provider Registration
Since, we have created our provider using cli, it is already included in `app.module.ts`:-
```typescript
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MessageController } from './message/message.controller';
import { MessageService } from './message/message.service';

@Module({
  imports: [],
  controllers: [AppController, MessageController],
  providers: [AppService, MessageService],
})
export class AppModule {}
```
otherwise, we need to link the providers in modules also.

## Refactor the code
Our `message` module has a number files, it will be helpful if we can refactor our project structure.
```
message
|-dto
|  |-create-message-DTO.ts
|
|-interface
|  |-message.interface.ts
|
|-message.controller.ts
|-message.controller.spec.ts
|-message.service.ts
|-message.service.spec.ts
```

# Modules

Modules are logical components in your project which implement a functionality for the project. For example, a project can have User Module, Orders Module and Chat Module who can also have child modules, each implementing a simple functionality.

## Create a new Module
Our chat Message module is already pretty bug, it can have it's own module. Again, for creating a module, you have two options:-
1. Create manually.
2. Create using cli
```
  nest g module Message
```
Now a `message.module.ts` is already created. Open, it and add the controller and service in it.
```typescript
import { Module } from '@nestjs/common';
import { MessageController } from './message.controller';
import { MessageService } from './message.service';

@Module({
    controllers: [MessageController],
    providers: [MessageService]
})
export class MessageModule { }

```

## Add module to App module
Now, you need to add `MessageModule` to `AppModule`. To do so, edit `app.module.ts`.
```typescript
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MessageModule } from './message/message.module';

@Module({
  imports: [MessageModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }

```
As you can see, we also removed `MessageController` and `MessageService` from `AppModule` as they are already the part of `MessageModule`.

## Shared Modules

In Nest, modules are singleton by nature, therefore, you can share same instance of `MessageService`(provider) between multiple module, for example, we can include `MessageService` in `AppModule` as well as `MessageModule` at the same time.

To do so, we need to export the `MessageService` in `MessageModule` as shown here:-
```typescript
import { Module } from '@nestjs/common';
import { MessageController } from './message.controller';
import { MessageService } from './message.service';

@Module({
    controllers: [MessageController],
    providers: [MessageService],
    exports: [MessageService],
})
export class MessageModule { }
``` 

## Module re-exporting

Similar to re-using provider, a module can export the module it is importing, for example,
```typescript
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MessageModule } from './message/message.module';

@Module({
  imports: [MessageModule],
  controllers: [AppController],
  providers: [AppService],
  exports: [MessageModule]
})
export class AppModule { }

``` 

## Dependency Injection

A module can inject its providers as well(for configuration purpose).
```typescript
import { Module } from '@nestjs/common';
import { MessageController } from './message.controller';
import { MessageService } from './message.service';

@Module({
    controllers: [MessageController],
    providers: [MessageService],
    exports: [MessageService],
})
export class MessageModule {
    constructor(private messageService: MessageService) { }
}

```
**Note:-** Module classes cannot be injected as themselves due to **circular dependency**.

## Global Modules

By Default, providers are not global in nature.

When you want provider to be global(for examples, helper functions, database connections etc.,) using `@Global` decorator.
```typescript
import { Global, Module } from '@nestjs/common';
import { MessageController } from './message.controller';
import { MessageService } from './message.service';

@Global()
@Module({
    controllers: [MessageController],
    providers: [MessageService],
    exports: [MessageService],
})
export class MessageModule {
    constructor(private messageService: MessageService) { }
}

```
Global modules need to be registered once, usually by root module

## Dynamic Modules

Dynamic Modules are the modules which can register and configure providers dynamically.
Consider this module,
```typescript
import { Module, DynamicModule } from '@nestjs/common';
import { createDatabaseProviders } from './database.providers';
import { Connection } from './connection.provider';

@Module({
  providers: [Connection],
})
export class DatabaseModule {
  static forRoot(entities = [], options?): DynamicModule {
    const providers = createDatabaseProviders(options, entities);
    return {
      module: DatabaseModule,
      providers: providers,
      exports: providers,
    };
  }
}

```

>`forRoot` can return dynamic module synchronously as well as asynchronously.

Here, `DatabaseModule` has `Connection` provider by default. But depending on entities and option, we are also providing providers. Instead of overriding `Connection`, it actually add `providers` along with the Connection.

If you want to register a dynamic global module, just add global property.
```txt
{
	global:true,
    module: DatabaseModule,
    providers: providers,
    exports: providers,
}
```
> **warning**
>
> Setting everything global is not a good descision.

Now, the `DatabaseModule` can be configured like:-
```typescript
import { Module } from '@nestjs/common';
import { DatabaseModule } from './database/database.module';
import { User } from './users/entities/user.entity';

@Module({
  imports: [DatabaseModule.forRoot([User])],
})
export class AppModule {}
```
If you want to re-export the dynamic module, you do not need to use `forRoot` again.
```typescript
import { Module } from '@nestjs/common';
import { DatabaseModule } from './database/database.module';
import { User } from './users/entities/user.entity';

@Module({
  imports: [DatabaseModule.forRoot([User])],
})
export class AppModule {}
```

# Middleware

Middleware are the functions which runs before route handlers. 

## How to create a middleware
There are two ways to create a middlewares:-
1. Manually
2. Using cli
    ```
    nest g middleware middleware-name
    ```
Let's create a new middleware called `logger.middleware.ts`. Open the file and you will see this code:-
```typescript
import { Injectable, NestMiddleware } from '@nestjs/common';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  use(req: any, res: any, next: () => void) {
    next();
  }
}

```
Here our `LoggerMiddleware` is implementing `NestMiddleware` middleware. The `use` method has three arguments:-
1. Request object
2. Response object
2. next method:- This method must be called after the middleware has finished it's operation to pass the request to other middleware/request handlers.

Let's create a simple functionality where it logs something on console with every request, the code will now look likt this:-

## Dependency Injection

Middleware supports dependency injection using constructor.

## Applying Middleware
To apply a middleware, we need to use `configure` method of `module` class but those modules needs to implement `NestModule` interface. We can now use this information to set up our `LoggerMiddleware`.
```typescript
import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { LoggerMiddleware } from './logger.middleware';
import { MessageModule } from './message/message.module';

@Module({
  imports: [MessageModule],
  controllers: [AppController],
  providers: [AppService],
  exports: [MessageModule]
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(LoggerMiddleware).forRoutes('message');
  }
}
```
We can also add a particular method to which a method is used.
```typescript
import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { LoggerMiddleware } from './logger.middleware';
import { MessageModule } from './message/message.module';

@Module({
  imports: [MessageModule],
  controllers: [AppController],
  providers: [AppService],
  exports: [MessageModule]
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(LoggerMiddleware).forRoutes({ path: 'message', method: RequestMethod.GET });
  }
}

```
> **Note**
>
> `Configure` method can be made async/await

Consumers can also have single/multiple controllers, like this,
```typescript
import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { AppController } from './app.controller';
import { MessageController } from './message/message.controller';
import { AppService } from './app.service';
import { LoggerMiddleware } from './logger.middleware';
import { MessageModule } from './message/message.module';

@Module({
  imports: [MessageModule],
  controllers: [AppController],
  providers: [AppService],
  exports: [MessageModule]
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(LoggerMiddleware).forRoutes(MessageController);
  }
}

```

## Excluding Middleware
We can easily exclude some routes using `exclude()`.
```typescript
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(LoggerMiddleware)
      .exclude({ path: "message", method: RequestMethod.GET })
      .forRoutes(MessageController);
  }
}
```

## Functional Middleware
Let's write our middleware using functions.
```typescript
import { Request, Response, NextFunction } from 'express';

export function logger(req: Request, res: Response, next: NextFunction) {
  console.log("Request...");
  next();
}
```
and use it like,
```typescript
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(logger)
      .exclude({ path: "message", method: RequestMethod.GET })
      .forRoutes(MessageController);
  }
}
```

## Multiple Middleware
We can declare multiple middleware like,
```typescript
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(LoggerMiddleware, logger)
      .exclude({ path: "message", method: RequestMethod.GET })
      .forRoutes(MessageController);
  }
}
```

## Global Middleware
We can make a middleware global using `.use` parameter.
```typescript
async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(logger);
  await app.listen(3000);
}
```
> **Hint**
>
> Accessing the **DI container**(A DI Container is a framework to create dependencies and inject them automatically when required) in a global middleware is not possible. You can use a functional middleware instead when using app.use(). Alternatively, you can use a class middleware and consume it with `.forRoutes('*')` within the AppModule (or any other module).

# Exception Filters
NestJs has it's own exception layer. When an exception is not handled by our code, nestjs send appropiate user-friendly response.

## Throwing standard exceptions
`HttpException` should be used to send HTTP response when error occurs.
```typescript
import {
    HttpCode,
    HttpException,
    HttpStatus,
} from '@nestjs/common';
@Controller('message')
export class MessageController {
    @Get()
    async listAll(@Req() request: Request): Promise<Message[]> {
        throw new HttpException('Forbidden', HttpStatus.FORBIDDEN);
    }
}
```
It will throw these message when visited,
```json
{
  "statusCode": 403,
  "message": "Forbidden"
}
```

## Overriding the response
It can be done like,
```typescript
@Get()
async listAll(@Req() request: Request): Promise<Message[]> {
    throw new HttpException({
        status: HttpStatus.FORBIDDEN,
        message: 'You are not allowed to access it'
    }, HttpStatus.FORBIDDEN);
}
```
It will produce this response,
```
{
  "status": 403,
  "error": 'You are not allowed to access it'
}
```

## Custom Exceptions
While creating custom exceptions, inherit from `HttpException` 
```typescript
export class ForbiddenException extends HttpException {
  constructor() {
    super('Forbidden', HttpStatus.FORBIDDEN);
  }
}
```
Then, you can use it like,
```typescript
@Get()
async findAll() {
  throw new ForbiddenException();
}
```

## Exception Filters
Suppose you need full control over exception layer. You might want to do some logging or use a different JSON based on some factors. Exception filters are used for this.

### How to create a Exception Filter
There are two ways to create Exception Filter:-
1. Manually
2. Using cli
    ```
    nest g filter filername
    ```
Let's create a filter with name `HttpExceptionFilter` and open it,
```typescript
import { ArgumentsHost, Catch, ExceptionFilter } from '@nestjs/common';

@Catch()
export class HttpExceptionFilter<T> implements ExceptionFilter {
  catch(exception: T, host: ArgumentsHost) {}
}
```
As you can see, our `HttpExceptionFilter` implements `ExceptionFilter` interface and provides `catch` function.
Let's implement it,
```typescript
import { 
    ArgumentsHost, 
    Catch, 
    ExceptionFilter, 
    HttpException 
} from '@nestjs/common';
import { Request, Response } from 'express';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();
    const status = exception.getStatus();

    response.status(status).json({
      statusCode: status,
      timestamp: new Date().toISOString(),
      path: request.url
    });
  }
}
```
### Binding Filters
We can bind filters with controller using `@UseFilters`.
```typescript
@Controller('message')
export class MessageController {

    @Get()
    @UseFilters(new HttpExceptionFilter())
    async listAll(@Req() request: Request): Promise<Message[]> {
        //return this.messageService.listAll();
        throw new HttpException({
            status: HttpStatus.FORBIDDEN,
            message: 'You are not allowed to access it'
        }, HttpStatus.FORBIDDEN);
    }
}
```
Ofcourse, you can apply the filter to container itself.
For using it globally, we can do it like,
```typescript
async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(logger);
  app.useGlobalFilters(new HttpExceptionFilter());
  await app.listen(3000);
}
```

# Pipes
Pipes have two uses:-
1. **Transformation:-** Transform input to desired form(for example, from string to integer).
2. **Validation:-** Evaluate the input data and if valid, simply pass it through, otherwise raise exception.

## Binding Pipes
We can bind Pipes like below,
```typescript
@Controller('message')
export class MessageController {

    @Get(':id')
    viewMessage(@Param('id',new ParseIntPipe({errorHt })) id: number): string {
        return `you are viewing ${id} message`;
    }
}
```
Here, we are ensuring that the `id` is always a number before reaching the controller, if it is not a controller, an exception will be raised before the method is called.
Now, if we access `/message/ab`, we will get this message,
```json
{
  "statusCode": 400,
  "timestamp": "2022-06-28T05:19:59.202Z",
  "path": "/message/ab"
}
```

We can also pass as in-place instance,
```typescript
@Get(':id')
    viewMessage(
        @Param('id', new ParseIntPipe({
            errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE
        })) id: number): string {
        return `you are viewing ${id} message`;
    }
```
## How to create Custom Pipes
There are two ways to create custom pipes:-
1. Manually
2. Using CLI
```
nest g pipe pipename
```
For example,
```
nest g pipe Validation
```
will create a pipe with name `ValidationPipe`.
Let's open `validation.pipe.ts` and see it's contents:-
```typescript
import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';

@Injectable()
export class ValidationPipe implements PipeTransform {
  transform(value: any, metadata: ArgumentMetadata) {
    return value;
  }
}
```
Currently, we are just returning the values.

## Object Schema Validation

`Joi` library allows us to create schemas. We will create a pipe which uses joi-based schema.
Let's install the package:-
```
npm install --save joi
npm install --save-dev @types/joi
```
Let's create a class that takes schema as `constructor`. Then, we are using `schema.validate()` method to validate the incoming schema with provided schema.
```typescript
import { ArgumentMetadata, BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { ObjectSchema } from 'joi';

@Injectable()
export class JoiValidationPipe implements PipeTransform {
  constructor(private schema: ObjectSchema) { }
  transform(value: any, metadata: ArgumentMetadata) {
    const { error } = this.schema.validate(value);
    if (error) {
      throw new BadRequestException("Request Failed!!");
    }
    return value;
  }
}

```

## Binding validation pipes

We can use `JoiValidationPipe` like any other pipe.
```typescript
@Post()
@UsePipes(new JoiValidationPipe(createMessageSchema))
@Header('Cache-Control', 'none')
@HttpCode(208)
async createMessage(@Body() createMessageDto: CreateMessageDto) {
    this.messageService.create(createMessageDto);
}
```

## Class Based Validator

Let's look at an alternative implmentation of validation. Let's install this package
```
npm i --save class-validator class-transformer
```
Let's open our `create-message-DTO.ts` and add some validation,
```typescript
export class CreateMessageDto {
    @IsInt()
    id: Number;
    @IsString()
    sender: String;
    @IsString()
    body: String;
    @IsString()
    receiver: String;
};
```
Let's take `ValidationPipe` which uses that annotations,
```typescript
import { ArgumentMetadata, BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { validate } from 'class-validator';

@Injectable()
export class ValidationPipe implements PipeTransform {
  private toValidate(metatype: Function): boolean {
    const types: Function[] = [String, Boolean, Number, Array, Object];
    return !types.includes(metatype);
  }
  async transform(value: any, { metatype }: ArgumentMetadata) {
    if (!metatype || !this.toValidate(metatype)) {
      return value;
    }
    const object = plainToClass(metatype, value);
    const errors = await validate(object);
    if (errors.length > 0) {
      throw new BadRequestException('Validation failed');
    }
    return value;
  }
}
```
Let's link this pipe to our controller in new way:-
```typescript
@Post()
@Header('Cache-Control', 'none')
@HttpCode(208)
async createMessage(
    @Body(new ValidationPipe()
) createMessageDto: CreateMessageDto) {
    this.messageService.create(createMessageDto);
}
```

## Global Pipes:-
```typescript
async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(logger);
  app.useGlobalFilters(new HttpExceptionFilter());
  app.useGlobalPipes(new ValidationPipe());
  await app.listen(3000);
}
```
If you want to include this pipe to your module, you can do it like,
```typescript
import { Global, Module, ValidationPipe } from '@nestjs/common';
import { APP_PIPE } from '@nestjs/core';
import { MessageController } from './message.controller';
import { MessageService } from './message.service';

@Global()
@Module({
    controllers: [MessageController],
    providers: [
        MessageService,
        {
            provide: APP_PIPE,
            useClass: ValidationPipe
        }
    ],
    exports: [MessageService],
})
export class MessageModule {
    constructor(private messageService: MessageService) { }
}
```

## Transformation Use Case
Pipes are not only used to validation, they can be used to transform the data, suppose you want to add some default values if they are missing in the data.
### Converting a string to Int
```typescript
import { PipeTransform, Injectable, ArgumentMetadata, BadRequestException } from '@nestjs/common';

@Injectable()
export class ParseIntPipe implements PipeTransform<string, number> {
  transform(value: string, metadata: ArgumentMetadata): number {
    const val = parseInt(value, 10);
    if (isNaN(val)) {
      throw new BadRequestException('Validation failed');
    }
    return val;
  }
}
```
**Usage:-**
```typescript
@Get(':id')
async findOne(@Param('id', new ParseIntPipe()) id) {
  return this.catsService.findOne(id);
}
```
### Providing default
```typescript
@Get()
async findAll(
  @Query('activeOnly', new DefaultValuePipe(false), ParseBoolPipe) activeOnly: boolean,
  @Query('page', new DefaultValuePipe(0), ParseIntPipe) page: number,
) {
  return this.catsService.findAll({ activeOnly, page });
}
```

# Guards
Guards have a single reponsibility. They determine whether a given request will be handled by route handler or not, depending on conditions like permissions, roles, ACLs etc.
But middleware, by its nature, is dumb. It doesn't know which handler will be executed after calling the `next()` function. On the other hand, Guards have access to the `ExecutionContext` instance, and thus know exactly what's going to be executed next. They're designed, much like exception filters, pipes, and interceptors, to let you interpose processing logic at exactly the right point in the request/response cycle, and to do so declaratively. This helps keep your code DRY and declarative.

> **Note:-**
>
> `Guards` run after all `middleware`, but before any `interceptor` or `pipe`.

## Authorization Guard:-
We will implement a Guard which will check for token in the request header and use the information to determine whether to proceed or not.

There are two ways to create guards:-
1. Manually
2. Using CLI
```
nest g guard guard-name
```
Let's create a guard called `Auth`.Open `auth.guard.ts` and examine it's content:-
```typescript
import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';

@Injectable()
export class AuthGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    return true;
  }
}
```
Here, our `AuthGuard` implements `CanActivate` interface. Our `AuthGuard` has a function `canActivate` which has `ExecutionContext` argument.
Let's implement it,
```typescript
import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { Request } from 'express';

@Injectable()
export class AuthGuard implements CanActivate {
  validateRequest(request: Request): boolean {
    //here we can have our logic based on the requirement
    return true;
  }
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();

    return this.validateRequest(request);
  }
}
```
If it returns:-
1. `true`:- request will be processed.
2. `false`:- request will be rejected.

## Role-Based Authentication
Let's create a role-based authentication which will only permit access for particular role.

```typescript
import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';

@Injectable()
export class RolesGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    return true;
  }
}
```

## Binding Guards
Simple, use the `@UseGuards()` decorator,
```typescript
@Controller('/message')
@UseGuards(RolesGuard)
export class CatsController {}
```
We can also bind using in-place instance.
```typescript
@Controller('/message')
@UseGuards(new RolesGuard())
export class CatsController {}
```
For binding globally, we can do it,
```typescript
app.useGlobalGuards(new RolesGuard());
```
We can also add the Guards in Modules.
```typescript
@Global()
@Module({
    controllers: [MessageController],
    providers: [
        MessageService,
        {
            provide: APP_PIPE,
            useClass: ValidationPipe
        }, {
            provide: APP_GUARD,
            useClass: RolesGuard,
        }
    ],
    exports: [MessageService],
})
export class MessageModule {
    constructor(private messageService: MessageService) { }
}
```
>**Hint:-**
>
>When using this approach to perform dependency injection for the guard, note that regardless of the module where this construction is employed, the guard is, in fact, global. Where should this be done? Choose the module where the guard (`RolesGuard` in the example above) is defined. Also, `useClass` is not the only way of dealing with custom provider registration.

## Setting Roles per handler

Nest provides the ability to add custom metadata to routes 
```typescript
@Post()
@Header('Cache-Control', 'none')
@SetMetadata('roles', ['admin'])
@HttpCode(208)
async createMessage(
    @Body(new ValidationPipe()
) createMessageDto: CreateMessageDto) {
    this.messageService.create(createMessageDto);
}
```
But this approach is not clean. We can create custom decorator using cli,
```
nest g decorator Roles
```
Let's create the decorator
```typescript
import { SetMetadata } from '@nestjs/common';

export const Roles = (...roles: string[]) => SetMetadata('roles', roles);
```
Now, we can modify the decorator in controller,
```typescript
@Post()
@Header('Cache-Control', 'none')
@Roles('admin')
@HttpCode(208)
async createMessage(
    @Body(new ValidationPipe()
) createMessageDto: CreateMessageDto) {
    this.messageService.create(createMessageDto);
}
```

## Putting it all together
We now want to now check the roles of user with the role needed to access the route,
We' will use `Reflector` class.
```typescript
import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable } from 'rxjs';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private reflector: Reflector) {
  }

  matchRoles(roles, userrole) {
    if (roles === userrole) {
      return true;
    }
    return false;
  }
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const roles = this.reflector.get<String[]>('roles', context.getHandler());
    if (!roles) {
      return true;
    }
    const request = context.switchToHttp().getRequest();
    const user = request.user;
    return this.matchRoles(roles, user.roles);
  }
}

```

# Interceptors
Interceptors have a set of useful capabilities which are inspired by the Aspect Oriented Programming (AOP) technique. They make it possible to:
* bind extra logic before / after method execution
* transform the result returned from a function
* transform the exception thrown from a function
* extend the basic function behavior
* completely override a function depending on specific conditions (e.g., for caching purposes)

## Basics
Each Interceptor implements `intercept()` method. It gets two arguments:-

1. **ExecutionContext:-** It extends `ArgumentsHost` while also adding several helpful methods which can be checked at official docs.

2. **CallHandler:-** `callHandler` interface implements `handle()` method, which can be used to invoke route handler function, if you don't call `handle` inside your `intercept`, route handler will not be executed. `handle` method also returns a observable which we can used to affect the response send by the handler.

## Create a Interceptor
1. Manually
2. Via cli
```
nest g interceptor interceptor-name
```

## Aspect Interception
```typescript
import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { Observable,tap } from 'rxjs';

@Injectable()
export class LoggingInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    console.log("Before ...");
    const now = Date.now();
    return next.handle().pipe(
      tap(()=>console.log(`After... ${Date.now()}ms}`))
    );
  }
}
```
First use case would be to log an user inteaction,
Since `handle()` returns an `RxJS` Observable, we have a wide choice of operators we can use to manipulate the stream. In the example above, we used the `tap()` operator, which invokes our anonymous logging function upon graceful or exceptional termination of the observable stream, but doesn't otherwise interfere with the response cycle.

## Binding Interceptors
Interceptors can be binded using `@UseInterceptors` and can be controller-scoped, method-scoped, module-scoped.
```typescript
@Controller('message')
@UseInterceptors(LoggingInterceptor)
export class MessageController {
}
```
Now, if you visit the `/message/1`, you will see,
![Our Interceptor is working](/images/Tutorials/NestJs/Interceptor.jpg)

## Resource Mapping
The `handle` method reuturns an `Observable`. The stream also contains value returned by the handler which we can easily mutate using `map` operator.

Let's create a `TransformInterceptor` which we will modify the response.
```typescript
import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { Observable,map } from 'rxjs';

export interface Response<T>{
  data:T,
};

@Injectable()
export class TransformInterceptor<T> implements NestInterceptor<T,Response<T>> {
  intercept(context: ExecutionContext, next: CallHandler): Observable<Response<T>> {
    return next.handle().pipe(map(data=>({data})));
  }
}
```

# Custom Route Decorators
Decorators is an expression which take arguments and returns functions. They are applied by prefixing `@`.

## How to create Decorator
1. Manually
2. Using Cli
```
nest g decorator decorator-name
```

Let's create a decorator called `User` which you can edit it like:-
```typescript
import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { userInfo } from 'os';

export const User = createParamDecorator(
    (data:unknown,ctx:ExecutionContext)=>{
        const request = ctx.switchToHttp().getRequest();
        return request.user;
    }
);

```
Now you can use this as shown below:-
```typescript
@Get()
@UseFilters(new HttpExceptionFilter())
async listAll(@Req() request: Request, @User() user): Promise<Message[]> {
    //return this.messageService.listAll();
    throw new HttpException({
        status: HttpStatus.FORBIDDEN,
        message: 'You are not allowed to access it'
    }, HttpStatus.FORBIDDEN);
}
```

Let's assume that our authentication layer validates a user and attaches a user object to request with following structure:-
```json
{
  "id": 101,
  "firstName": "Alan",
  "lastName": "Turing",
  "email": "alan@email.com",
  "roles": ["admin"]
}
```

Let's modify our decorator so that it checks for a property name as input and returns it's value if present, otherwise none.
```typescript
export const User = createParamDecorator(
    (data:string,ctx:ExecutionContext)=>{
        const request = ctx.switchToHttp().getRequest();
        const user = request.user;
        return data?user?.[data]:user;
    }
);
```

We can use it like this,
```
@Get()
@UseFilters(new HttpExceptionFilter())
async listAll(@Req() request: Request, @User('firstName')firstName: string): Promise<Message[]> {
    console.log(firstName);
    //return this.messageService.listAll();
    throw new HttpException({
        status: HttpStatus.FORBIDDEN,
        message: 'You are not allowed to access it'
    }, HttpStatus.FORBIDDEN);
}
```
## Working with Pipes
Pipes can be used seemlessly even with our custom decorators.
```typescript
@Get()
async findOne(
  @User(new ValidationPipe({ validateCustomDecorators: true }))
  user: UserEntity,
) {
  console.log(user);
}
```
## Decorator Composition
We can combine multiple decorators into one.
```typescript
import { applyDecorators,SetMetadata,UseGuards } from '@nestjs/common';
import { AuthGuard } from '../guards/auth.guard';
import { RolesGuard } from '../guards/roles.guard';

export function Auth(...roles: []) {
  return applyDecorators(
    SetMetadata('roles', roles),
    UseGuards(AuthGuard, RolesGuard)
  );
}
```