import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { AppController } from './app.controller';
import { MessageController } from './message/message.controller';
import { AppService } from './app.service';
import { logger, LoggerMiddleware } from './message/middleware/logger.middleware';
import { MessageModule } from './message/message.module';

@Module({
  imports: [MessageModule],
  controllers: [AppController],
  providers: [AppService],
  exports: [MessageModule]
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(LoggerMiddleware, logger)
      .exclude({ path: "message", method: RequestMethod.GET })
      .forRoutes(MessageController);
  }
}
