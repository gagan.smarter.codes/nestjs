import { isDate, IsInt, IsNumber, IsString } from "class-validator";

export class CreateMessageDto {
    @IsInt()
    id: Number;
    @IsString()
    sender: String;
    @IsString()
    body: String;
    @IsString()
    receiver: String;
};