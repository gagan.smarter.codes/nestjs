import { Injectable } from '@nestjs/common';
import { Message } from './interface/message.interface';
@Injectable()
export class MessageService {
    private readonly messages: Message[] = [];

    create(message: Message) {
        this.messages.push(message);
    }

    listAll(): Message[] {
        return this.messages;
    }
}
