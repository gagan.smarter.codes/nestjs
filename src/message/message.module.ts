import { Global, Module, ValidationPipe } from '@nestjs/common';
import { APP_GUARD, APP_PIPE } from '@nestjs/core';
import { RolesGuard } from 'src/message/guards/roles.guard';
import { MessageController } from './message.controller';
import { MessageService } from './message.service';

@Global()
@Module({
    controllers: [MessageController],
    providers: [
        MessageService,
        {
            provide: APP_PIPE,
            useClass: ValidationPipe
        }, {
            provide: APP_GUARD,
            useClass: RolesGuard,
        }
    ],
    exports: [MessageService],
})
export class MessageModule {
    constructor(private messageService: MessageService) { }
}
