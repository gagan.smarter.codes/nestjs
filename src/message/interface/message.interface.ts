export interface Message {
    sender: String;
    body: String;
    receiver: String;
}