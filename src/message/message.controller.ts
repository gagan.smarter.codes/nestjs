import {
    Body,
    Controller,
    Get,
    Header,
    HttpCode,
    HttpException,
    HttpStatus,
    Param,
    ParseIntPipe,
    Post,
    Req,
    SetMetadata,
    UseFilters,
    UseInterceptors,
    UsePipes
} from '@nestjs/common';
import { Request } from 'express';
import { CreateMessageDto } from './dto/create-message-DTO';
import { MessageService } from './message.service';
import { Message } from './interface/message.interface';
import { HttpExceptionFilter } from 'src/message/filters/http-exception.filter';
import { JoiValidationPipe } from 'src/message/pipe/joi-validation.pipe';
import { ValidationPipe } from 'src/message/pipe/validation.pipe';
import { Roles } from 'src/message/decorators/roles.decorator';
import { LoggingInterceptor } from 'src/message/interceptors/logging.interceptor';
import { User } from 'src/message/decorators/user.decorator';

@Controller('message')
@UseInterceptors(LoggingInterceptor)
export class MessageController {

    //dependency injection
    constructor(private messageService: MessageService) { }

    @Post()
    @Header('Cache-Control', 'none')
    @Roles('admin')
    @HttpCode(208)
    async createMessage(
        @Body(new ValidationPipe()
        ) createMessageDto: CreateMessageDto) {
        this.messageService.create(createMessageDto);
    }

    @Get()
    @UseFilters(new HttpExceptionFilter())
    async listAll(@Req() request: Request, @User() user): Promise<Message[]> {
        //return this.messageService.listAll();
        throw new HttpException({
            status: HttpStatus.FORBIDDEN,
            message: 'You are not allowed to access it'
        }, HttpStatus.FORBIDDEN);
    }

    @Get(':id')
    viewMessage(
        @Param('id', new ParseIntPipe({
            errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE
        })) id: number): string {
        return `you are viewing ${id} message`;
    }
}
